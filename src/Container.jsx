import React, { Component } from 'react';
import axios from 'axios';
import Question from './Question';
import Report from './Report';
const QuestionFetchApiEndpoint = 'https://cdn.rawgit.com/santosh-suresh/39e58e451d724574f3cb/raw/784d83b460d6c0150e338c34713f3a1c2371e20a/assignment.json';

class Container extends Component {
    /* Initialize States
        questionList state maintain the Questions data fetched by Api
        currentQuestionIndex state point the curent question
        answerList state maintain the selected usesr option
        modalShow state maintain the state of report 
     */    
    state = {
        questionList: [],
        currentQuestionIndex: 0,
        answerList: [],
        modalShow: false
    }

    componentDidMount() {
        // To fetch the question data from Api
        axios.get(QuestionFetchApiEndpoint)
            .then(response => this.setState({ questionList: response.data }))
            .catch(err => console.log)
    }

    // Handler to handle answer choose
    handler = (value) => {
        let { questionList, currentQuestionIndex, answerList } = this.state;
        if (questionList && currentQuestionIndex + 1 < questionList.length) {
            answerList.push({ correctAnswer: questionList[currentQuestionIndex].answer, selectedOption: value });
            this.setState({ currentQuestionIndex: currentQuestionIndex + 1, answerList })
        } else if (questionList && currentQuestionIndex + 1 === questionList.length && answerList.length !== questionList.length) {
            answerList.push({ correctAnswer: questionList[currentQuestionIndex].answer, selectedOption: value });
            this.setState({ currentQuestionIndex: currentQuestionIndex, answerList});
            this.setModalShow(true);
        }
    }
    // update the report open state
    setModalShow = (state) => {
        this.setState({modalShow: state})
    }
    // Reset the container state to replay the puzzle
    resetPuzzle = () => {
        this.setState({currentQuestionIndex: 0,answerList: [],modalShow: false})
    }
    
    render() {
        const { questionList, currentQuestionIndex, modalShow, answerList } = this.state;
        return (
            <div className="app">
                {
                    questionList.length ?
                        <Question
                            data={questionList[currentQuestionIndex]}
                            total={questionList.length}
                            current={currentQuestionIndex + 1}
                            handler={this.handler}
                        />
                        : null
                }
                <Report show={modalShow} answerlist={answerList} onHide={() => this.resetPuzzle()}/>
            </div>
        );
    }
}

export default Container;
