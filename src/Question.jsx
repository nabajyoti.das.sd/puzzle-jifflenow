import React from 'react';

function Question(props){
    const { data, total, current, handler } = props;
    return (
        data ?
        <div className="px-5 pt-2 pb-5 text-white position-relative h-100">
            <div>
                Javascript Quiz {current} of {total}
            </div>
            <h5 className="mt-1 mb-4">{data.text}</h5>
            <ol>{data.options.map(ele => <li type="A" key={ele}>{ele.toUpperCase()}</li>)}</ol>
            <div className="answer-button d-flex justify-content-center">
                {data.options.map((ele, index) => <button className="btn bg-white mx-4 px-4" key={ele} onClick={() => handler(index)}>{ String.fromCharCode(65+index)}</button>)}
            </div> 
        </div> : null
    )
}

export default Question;