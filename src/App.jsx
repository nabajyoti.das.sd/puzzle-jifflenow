import React from 'react';
import Container from './Container';
import './App.css';

function App() {
  return (
    // Main Container Component
    <Container/>
  );
}

export default App;
