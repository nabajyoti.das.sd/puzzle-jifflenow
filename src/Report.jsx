import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { CircularProgressbar } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';

function Report(props) {
    const {answerlist} = props;
    const reportData = getReportData(answerlist);
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered keyboard={false} backdrop="static">
            <Modal.Header className="border-bottom-0">
                <Modal.Title className="w-100 text-center text-primary" id="contained-modal-title-vcenter">
                    Report Card
                </Modal.Title>
            </Modal.Header>
            <Modal.Body className="border-bottom-0 text-center">
                <CircularProgressbar className="w-25 my-2" value={reportData.percentage} text={`${reportData.percentage}%`} />
                <h4>Total number of Question: {reportData.totalNumberOfQuestion}</h4>
                <h4>Right Atempts: {reportData.totalNumberOfCorrectAns}</h4>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={props.onHide}>Replay</Button>
            </Modal.Footer>
        </Modal>
    )
}
// answerList params is the user snapshot of the selected options
function getReportData(answerList) {
    let totalNumberOfQuestion = answerList.length;
    let totalNumberOfCorrectAns = answerList.filter((answer)=> answer.correctAnswer === answer.selectedOption).length;
    let percentage = Math.floor((totalNumberOfCorrectAns / totalNumberOfQuestion) * 100);
    return {totalNumberOfQuestion,totalNumberOfCorrectAns,percentage};
}

export default Report;